package HashTable;

public class Container<K, V>{
    K key;
    V value;

    //------------------------------------------------------------------------------------------------------------------
    //constructor
    public Container(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
