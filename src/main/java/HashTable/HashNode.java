package HashTable;

public class HashNode {
    Container currentObject;
    HashNode nextNode;
    HashNode previousNode;

    //------------------------------------------------------------------------------------------------------------------
    //constructor
    public HashNode(Container obj, HashNode previous) {
        this.currentObject = obj;
        this.previousNode = previous;
    }
}
