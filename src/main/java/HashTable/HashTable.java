package HashTable;

public class HashTable {

    //------------------------------------------------------------------------------------------------------------------
    //initialize the hash table
    //ArrayList<Object> hashTable = new ArrayList<Object>();
    HashNode[] hashTable = new HashNode[constants.hTLength];

    //------------------------------------------------------------------------------------------------------------------
    //takes a sampleObject (with both key and value as strings) and returns an index
    private int hashFunction(String... key) {

        //when the key is O1 to O9
        int index;
        index = ((int) key[0].charAt(1) - 48);

        //when the key is bigger then O9
        for (int i = 2, l = key[0].length(); i < l; i++) {
            index = index * 10 + ((int) key[0].charAt(i) - 48);
        }

        return index % constants.hTLength;
    }

    //------------------------------------------------------------------------------------------------------------------
    //return the value corresponding to a specific key
    public String get(String key) {

        HashNode node = getNode(key);

        Container<String, String> result = (node != null) ? node.currentObject : null;

        return (result == null) ? null : result.value;
    }

    //------------------------------------------------------------------------------------------------------------------
    //return the object which has the respective key
    private HashNode getNode(String key) {

        //get index
        int index = hashFunction(key);

        //get node
        if (hashTable[index] != null) {
            //searched through the linked list
            return searchLinkedList(hashTable[index], key);

        } else {
            //if the sought node is missing
            return null;
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //returns the size of the HT
    public int getSize() {

        int size = 0;

        //count the elements in every linked list
        for (HashNode node : hashTable) {
            size = countLinkedList(node, size);
        }

        return size;
    }

    //------------------------------------------------------------------------------------------------------------------
    //returns the number of nodes in the linked list
    private int countLinkedList(HashNode node, int counter) {

        return (node == null) ? counter : countLinkedList(node.nextNode, ++counter);

    }

    //------------------------------------------------------------------------------------------------------------------
    //double the hash table
    private void doubleHT() {
        if (constants.htSize / constants.hTLength > 0.7) {
            //if the hash table is 70% full

            //create a new hast table with a doubled size
            constants.hTLength *= 2;
            HashNode[] temp = hashTable;
            hashTable = new HashNode[constants.hTLength];

            //iterate through all the objects from the old HT and add them to the new one
            for (HashNode i : temp) {
                //initialize a iterative node
                HashNode node = i;

                copyNodes(node);

            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //runs through the linked list and copies all the nodes to the hash table
    private void copyNodes(HashNode node) {

        if (node != null) {
            //copy the current node to the hash table
            Container<String, String> obj = node.currentObject;
            add(obj.key, obj.value);

            //make a recursive call upon the other nodes in the linked list
            copyNodes(node.nextNode);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //adds the specified object to the HT
    public void add(String key, String value) {

        //create the object
        Container<String, String> obj = new Container<String, String>(key, value);

        int index = hashFunction(obj.key);

        if (hashTable[index] == null) {
            //if the new node is inserted as the first element of the linked list
            hashTable[index] = new HashNode (obj, null);

        } else {
            //if the new node is somewhere down the linked list
            hashTable[index] = addObject(hashTable[index], obj);
        }

        //sign the increase of the hash table
        constants.htSize++;
        //double the hash table if it got full up to 70%
        doubleHT();
    }

    //------------------------------------------------------------------------------------------------------------------
    //removes a node from the HT
    public void remove(String key) {

        HashNode node = getNode(key);

        //go to the linked list where the object is located and remove the object
        int index = hashFunction(key);
        hashTable[index] = removeNode(node);
    }

    //------------------------------------------------------------------------------------------------------------------
    //remove the current node and return the head of the linked list
    private HashNode removeNode(HashNode node) {

        if (node == null) {
            //if there is no linked list
            return null;

        } else {
            //if there is a linked list

            if (node.nextNode == null) {
                //if there are no more nodes down the line

                if (node.previousNode == null) {
                    //if this node is the only element in the linked list
                    return null;

                } else {
                    //if there are other nodes before this one in the linked list

                    node.previousNode.nextNode = null;
                    return linkedListRoot(node.previousNode);
                }

            } else {
                //if there are more nodes down the line

                if (node.previousNode == null) {
                    //if this is the root of the linked list

                    node.nextNode.previousNode = null;
                    return node.nextNode;
                } else {
                    //if this is not the root and neither the end

                    node.previousNode.nextNode = node.nextNode;
                    node.nextNode.previousNode = node.previousNode;
                    return linkedListRoot(node.previousNode);
                }
            }
        }

    }

    //------------------------------------------------------------------------------------------------------------------
    //return the root of a linked list
    private HashNode linkedListRoot(HashNode node) {

        //node.previousNode == null is the base case
        return (node.previousNode == null) ? node : linkedListRoot(node.previousNode);
    }

    //------------------------------------------------------------------------------------------------------------------
    //checks if this node is the one we are looking for and if not, checks the children until it gets to our node or returns null
    private HashNode searchLinkedList(HashNode node, String key) {
        if (node == null) {
            //if this node is null
            return null;

        } else {
            //if this node is not empty

            Container<String, String> currentObj = node.currentObject;

            if (currentObj.key.toLowerCase().equals(key.toLowerCase())) {
                //if the current node has the key we gave in
                return node;
            } else {
                //if this node is not the one we are searching
                return searchLinkedList(node.nextNode, key);
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //create a node in the linked list and put the object inside
    private HashNode addObject(HashNode node, Container obj) {
        if (node.nextNode == null) {
            //if the new node is going to be after this node

            node.nextNode = new HashNode(obj, node);

        } else {
            //if the new node is going to be somewhere down the line

            addObject(node.nextNode, obj);
        }

        return linkedListRoot(node);
    }

    //------------------------------------------------------------------------------------------------------------------
}

