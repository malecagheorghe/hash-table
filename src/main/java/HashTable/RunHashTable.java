package HashTable;

import java.util.ArrayList;

public class RunHashTable {

    //run the hash table user interface in the terminal
    public static void main(String[] args) {

        HashTable hash = new HashTable();
        ArrayList<Container> insertedObjects = new ArrayList<Container>();

        //create a hash table
        for (int i = 0; i < 100; i += 3) {
            //create the key
            String name = "O" + i;

            //add object to the hash table
            hash.add(name, "Value of " + name);

            //System.out.println("Added key: " + sObj.key);

            //save the object to the reference array (for testing purposes)
            //insertedObjects = extendArray(insertedObjects, sObj);
            insertedObjects.add(new Container <String, String> (name, "Value of " + name));
        }

        //show the hash table
        for (Container<String, String> i : insertedObjects) {
            System.out.println(i.key + ": " + hash.get(i.key));
        }

        //System.out.println(hash.get("o4"));

    }

    //------------------------------------------------------------------------------------------------------------------
    //extend the array by one and appends the object to the end
    private static Container[] extendArray(Container[] array, Container obj) {
        //create the new array
        int l = array.length;
        Container[] newArray = new Container[l + 1];

        //copy the old array into the new one
        for (int i = 0; i < l; i++) {
            newArray[i] = array[i];
        }

        //insert the object in the new array
        newArray[l] = obj;

        return newArray;
    }
}
